package com.example.fahriyalafif.belajaryuk;

import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fahriyalafif.belajaryuk.utility.DBManager;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private LinearLayout layout_soal;
    private View periksa_jawaban, next, prev;
    private int warna_ganjil = Color.parseColor("#90C3D4"), warna_genap = Color.parseColor("#D0EDF7");
    private int page = 1, per_page = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        layout_soal = (LinearLayout) findViewById(R.id.layout_soal);
        periksa_jawaban = findViewById(R.id.periksa_jawaban);
        next = findViewById(R.id.next);
        prev = findViewById(R.id.prev);

        periksa_jawaban.setOnClickListener(this);
        next.setOnClickListener(this);
        prev.setOnClickListener(this);

        loadSoal(1);
    }

    private void loadSoal(int halaman){
        page = halaman;
        layout_soal.removeAllViews();
        int num = ((page - 1) * per_page) + 1;
        Cursor cc = DBManager.getInstance(this).getSoal(page, per_page);

        while (cc.moveToNext()){
            View per_soal = getLayoutInflater().inflate(R.layout.per_soal, null);
            layout_soal.addView(per_soal);

            TextView no = (TextView) per_soal.findViewById(R.id.nomor);
            TextView pertanyaan = (TextView) per_soal.findViewById(R.id.pertanyaan);
            ImageView pertanyaan_gambar = (ImageView) per_soal.findViewById(R.id.pertanyaan_gambar);
            RadioGroup grup_pilihan = (RadioGroup) per_soal.findViewById(R.id.grup_pilihan);
            RadioButton rba = (RadioButton) per_soal.findViewById(R.id.pilihanA);
            RadioButton rbb = (RadioButton) per_soal.findViewById(R.id.pilihanB);
            RadioButton rbc = (RadioButton) per_soal.findViewById(R.id.pilihanC);

            no.setText(num+".");
            no.setTag(num+"");

            pertanyaan.setText(cc.getString(1));
            byte[] image = cc.getBlob(2);
            if(image != null) {
                pertanyaan_gambar.setVisibility(View.VISIBLE);
                pertanyaan_gambar.setImageBitmap(BitmapFactory.decodeByteArray(image, 0, image.length));
            }
            grup_pilihan.setTag(cc.getString(3));
            rba.setText(cc.getString(4));
            rbb.setText(cc.getString(5));
            rbc.setText(cc.getString(6));

            per_soal.setBackgroundColor(num % 2 == 0 ? warna_genap : warna_ganjil);

            num++;
        }
        cc.close();
    }

    @Override
    public void onClick(View v) {
        if(v == periksa_jawaban){
            String notif = "";
            for(int i=0; i<layout_soal.getChildCount(); i++){
                View per_soal = layout_soal.getChildAt(i);

                TextView no = (TextView) per_soal.findViewById(R.id.nomor);
                RadioGroup grup_pilihan = (RadioGroup) per_soal.findViewById(R.id.grup_pilihan);
                int id_radio_dipilih = grup_pilihan.getCheckedRadioButtonId();

                String kunci = grup_pilihan.getTag().toString();
                String jawaban = id_radio_dipilih == -1 ? "" : grup_pilihan.findViewById(id_radio_dipilih).getTag().toString();

                String status_jawaban = kunci.equalsIgnoreCase(jawaban) ? "BENAR" : "SALAH";

                notif = notif +"\n"+"No "+no.getTag().toString()+" => "+status_jawaban;
            }
            Toast.makeText(this, notif.trim(), Toast.LENGTH_LONG).show();
        }
        else if(v == next){
            int next_page = page + 1;
            boolean ada_soal = DBManager.getInstance(this).getSoal(next_page, per_page).getCount() > 0;
            if(ada_soal) loadSoal(next_page);
            else Toast.makeText(this, "Sudah di halaman terakhir", Toast.LENGTH_LONG).show();
        }
        else if(v == prev){
            int next_page = page - 1;
            if(next_page <= 0) Toast.makeText(this, "Sudah di halaman awal", Toast.LENGTH_LONG).show();
            else loadSoal(next_page);
        }
    }
}
