package com.example.fahriyalafif.belajaryuk.utility;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.fahriyalafif.belajaryuk.R;

import java.io.ByteArrayOutputStream;

/*
 * Created by Fahriyal Afif on 5/26/2016.
 */
public class DBManager extends SQLiteOpenHelper{
    private static final String DB_NAME = "my_db.db";
    private static final int DB_VERSION = 1;
    private static final String TABEL = "soal";

    private static DBManager main;
    private static SQLiteDatabase sqLiteDatabase;
    private Context context;

    public static DBManager getInstance(Context c){
        if(main == null) {
            main = new DBManager(c);
            sqLiteDatabase = main.getWritableDatabase();
        }

        return main;
    }

    private DBManager(Context c) {
        super(c, DB_NAME, null, DB_VERSION);
        context = c;
    }

    public synchronized long insertSoal(String pertanyaan, byte[] gambar, String kunci,
                                        String pilihanA, String pilihanB, String pilihanC){
        ContentValues v = new ContentValues();
        v.put("pertanyaan", pertanyaan);
        v.put("gambar", gambar);
        v.put("kunci", kunci);
        v.put("pilihanA", pilihanA);
        v.put("pilihanB", pilihanB);
        v.put("pilihanC", pilihanC);

        return sqLiteDatabase.insert(TABEL, "", v);
    }

    private synchronized void versi1(SQLiteDatabase dbn){
        String sql_tabel = "CREATE TABLE "+TABEL+" ( "
                + "id_soal INTEGER PRIMARY KEY, "
                + "pertanyaan TEXT, "
                + "gambar BLOB, "
                + "kunci TEXT, "
                + "pilihanA TEXT, "
                + "pilihanB TEXT, "
                + "pilihanC TEXT)";

        dbn.execSQL(sql_tabel);

        sqLiteDatabase = dbn;
        insertSoal("1 + 1 = ?", null, "a", "2", "3", "4");
        insertSoal("Kaki kucing berjumlah berapa ? ", null, "b", "5", "4", "3");
        insertSoal("Paus bernafas dengan ? ", null, "c", "insang", "jantung", "paru paru");
    }

    @Override
    public synchronized void onCreate(SQLiteDatabase dbn) {
        versi1(dbn);
    }

    @Override
    public synchronized void onUpgrade(SQLiteDatabase dbn, int oldVersion, int newVersion) {

    }

    public synchronized Cursor getSoal(int page, int count){
        int start = (page - 1) * count;
        return sqLiteDatabase.query(TABEL, null, null, null, null, null, null, start+", "+count);
    }

    /*private byte[] getByteArray(int resImage){
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resImage);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }*/

    /*@Override
    public synchronized void onUpgrade(SQLiteDatabase dbn, int oldVersion, int newVersion) {
        switch (oldVersion){
            case 1:
                versi2(dbn);
                break;
        }
    }*/

    /*private synchronized void versi2(SQLiteDatabase dbn){
        sqLiteDatabase = dbn;
        insertSoal("Perhatikan gambar berikut, gambar apakah itu?", getByteArray(R.drawable.buy), "c", "orang makan nasi",
                "orang berendara mobil", "tangan mengklik tombol buy");
        insertSoal("4 + 4 = ?", null, "a", "8", "10", "12");
        insertSoal("4 + 4 = ?", null, "a", "8", "10", "12");
        insertSoal("4 + 4 = ?", null, "a", "8", "10", "12");
        insertSoal("4 + 4 = ?", null, "a", "8", "10", "12");
        insertSoal("4 + 4 = ?", null, "a", "8", "10", "12");
        insertSoal("4 + 4 = ?", null, "a", "8", "10", "12");
        insertSoal("4 + 4 = ?", null, "a", "8", "10", "12");
        insertSoal("4 + 4 = ?", null, "a", "8", "10", "12");
        insertSoal("4 + 4 = ?", null, "a", "8", "10", "12");
    }*/

}
